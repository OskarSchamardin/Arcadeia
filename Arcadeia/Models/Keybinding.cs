using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arcadeia.Models
{
    class Keybinding
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ConsoleKey Key { get; set; }
    }
}

