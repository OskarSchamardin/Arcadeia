﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arcadeia.Models;

namespace Arcadeia
{
    class Program
    {
        /* ---[ Usings ]--- */
        /* -------------------------------------------------- */
        /* ---[ Notes ]--- */


        /* Note to self: this project is a side scrolling space shooter. I came up with the idea when I wanted
        to install a similar game but couldn't be bothered with dependencies and the fact it required a package
        manager command to run it. I wanted to challenge my coding skills and the power of a terminal.
        (-Oskar, Original Creator) */


        /* ---[ Notes ]--- */
        /* -------------------------------------------------- */
        /* ---[ Global Variables ]--- */

        /* Default Options */
        public static bool ShowDebugInfo = false;

        /* Important Variables */
        public static int WinX = Console.WindowWidth;
        public static int WinY = Console.WindowHeight;
        public static int PlayerX;
        public static int PlayerY;

        /* UI Decorations */
        public static string DecorWest = "---| ";
        public static string DecorEast = new string(DecorWest.Reverse().ToArray());
        public static string Top = DecorWest + "ARCADEIA" + DecorEast;

        /* Menus */
        public static Menu SelectedMenuItem;
        public static Menu CurrentMenu;

        public static List<Menu> MenuItems = new List<Menu>();
        public static List<Menu> Menus = new List<Menu>();

        public static Menu MainMenu;
        public static Menu NewGameMenu;
        public static Menu QuitGameMenu;
        public static Menu BackMenu;
        public static Menu OptionsMenu;
        public static Menu ToggleDebugMenu;
        public static Menu GamePlayScreen;

        /* Keybindings */
        public static List<Keybinding> Keybindings = new List<Keybinding>();

        public static Keybinding QuitKey;
        public static Keybinding NewGameKey;
        public static Keybinding BackMenuKey;
        public static Keybinding OptionsMenuKey;
        public static Keybinding DebugToggleKey;
        public static Keybinding MenuUpKey;
        public static Keybinding MenuDownKey;
        public static Keybinding MenuEnterKey;

        /* ---[ Global Variables ]--- */
        /* -------------------------------------------------- */
        /* ---[ Main Setup ]--- */

        static void Main(string[] args)
        {
            PopulateListsAndSetGlobalValues();
            CurrentMenu = MainMenu;

            /* When we get a interrupt signal */
            Console.CancelKeyPress += delegate
            {
                /* Clear Console for the user's convenience */
                if (!ShowDebugInfo)
                {
                    Console.Clear();
                }

                /* show curosr for the convenience of the user */
                Console.CursorVisible = true;
            };

            /* ---[ Main Setup ]--- */
            /* -------------------------------------------------- */
            /* ---[ Main Loop ]--- */

            /* It is not intended to have this loop be broken. Alternative is Quitting the game */
            do
            {
                Console.Clear();
                LoadMenu();
                DealWithUserMenuInput((ConsoleKey)PromptUserInput());
            }
            while (true);
        }

        /* ---[ Main Loop ]--- */
        /* -------------------------------------------------- */
        /* ---[ Functions ]--- */

        /* Populate Lists And Set Global Values*/
        static void PopulateListsAndSetGlobalValues()
        {
            /* Create Menus */
            Menus.Add(new Menu { Id=0, Name="Main Menu" });
            Menus.Add(new Menu { Id=1, Name="(N)ew Game" });
            Menus.Add(new Menu { Id=2, Name="(Q)uit" });
            Menus.Add(new Menu { Id=3, Name="(B)ack" });
            Menus.Add(new Menu { Id=4, Name="(O)ptions" });
            Menus.Add(new Menu { Id=5, Name="Toggle (D)ebug" });
            Menus.Add(new Menu { Id=6, Name="In Game" });

            /* Menu Shortcut Keys */
            Keybindings.Add(new Keybinding { Id=0, Name="QuitKey",          Key=ConsoleKey.Q });
            Keybindings.Add(new Keybinding { Id=1, Name="NewGameKey",       Key=ConsoleKey.N });
            Keybindings.Add(new Keybinding { Id=2, Name="BackMenuKey",      Key=ConsoleKey.B });
            Keybindings.Add(new Keybinding { Id=3, Name="OptionsMenuKey",   Key=ConsoleKey.O });
            Keybindings.Add(new Keybinding { Id=4, Name="DebugToggleKey",   Key=ConsoleKey.D });
            /* Menu Navigation Keys */
            Keybindings.Add(new Keybinding { Id=5, Name="MenuUpKey",        Key=ConsoleKey.K });
            Keybindings.Add(new Keybinding { Id=6, Name="MenuDownKey",      Key=ConsoleKey.J });
            Keybindings.Add(new Keybinding { Id=7, Name="MenuEnterKey",     Key=ConsoleKey.Enter });

            /* Set Values For Easier Reference */
            MainMenu        = Menus[0];
            NewGameMenu     = Menus[1];
            QuitGameMenu    = Menus[2];
            BackMenu        = Menus[3];
            OptionsMenu     = Menus[4];
            ToggleDebugMenu = Menus[5];
            GamePlayScreen  = Menus[6];

            QuitKey         = Keybindings[0];
            NewGameKey      = Keybindings[1];
            BackMenuKey     = Keybindings[2];
            OptionsMenuKey  = Keybindings[3];
            DebugToggleKey  = Keybindings[4];
            MenuUpKey       = Keybindings[5];
            MenuDownKey     = Keybindings[6];
            MenuEnterKey    = Keybindings[7];
        }

        /* -------------------------------------------------- */

        /* Draw menus before/after gameplay */
        static void LoadMenu()
        {
            MenuItems.Clear();
            DrawDebugInfo();

            ToggleDebugMenu.Name= "Toggle (D)ebug";

            if(ShowDebugInfo)
            {
                ToggleDebugMenu.Name+= " = On ";
            }
            else
            {
                ToggleDebugMenu.Name += " = Off";
            }

            /* Displayed lines */
            List<string> Lines = new List<string>();

            /* Header */
            Lines.Add(Top);
            Lines.Add(CurrentMenu.Name);
            Lines.Add("");

            if(CurrentMenu == MainMenu)
            {
                MenuItems.Add(NewGameMenu);
                MenuItems.Add(OptionsMenu);
                MenuItems.Add(QuitGameMenu);
            }
            else if(CurrentMenu == OptionsMenu)
            {
                MenuItems.Add(ToggleDebugMenu);
                MenuItems.Add(BackMenu);
            }
            else if(CurrentMenu == GamePlayScreen)
            {
            }

            /* Select first MenuItem if nothing is selected */
            if(!MenuItems.Contains(SelectedMenuItem))
            {
                SelectedMenuItem = MenuItems[0];
            }

            /* Selectable items */
            foreach(var MenuItem in MenuItems)
            {
                if(SelectedMenuItem == MenuItem)
                {
                    Lines.Add("--> " + MenuItem.Name + " <--");
                }
                else
                {
                    Lines.Add(MenuItem.Name);
                }
            }

            /* Draw menu screen */
            DrawLines(Lines);
        }

        /* -------------------------------------------------- */

        /* Draw the menu line by line while keeping it all centered in the terminal */
        static void DrawLines(List<string> Lines)
        {
            int counter = 0;
            foreach(var Line in Lines)
            {
                /* Center To Screen */
                Console.SetCursorPosition((WinX - Line.Length) / 2, WinY / 2 + counter - Lines.Count / 2);
                Console.WriteLine(Line);

                /* this counter is here to act as a line break to prevent
                lines getting overwritten by the next one */
                ++counter;
            }
        }

        /* -------------------------------------------------- */

        /* We want single keypresses from user */
        static char PromptUserInput()
        {
            /* We Use ReadKey(true) here because it does not print user keypresses */
            ConsoleKey c = Console.ReadKey(true).Key;
            return((char)c);
        }

        /* -------------------------------------------------- */

        /* We want to do something with user input (Keybingings at the top of the file) */
        static void DealWithUserMenuInput(ConsoleKey c)
        {
            /* Application Exits */
            if(c == QuitKey.Key || c == MenuEnterKey.Key && SelectedMenuItem == QuitGameMenu)
            {
                Console.Clear();
                Console.CursorVisible = true;
                Environment.Exit(0);
            }
            /* New Game Screen */
            else if(c == NewGameKey.Key || c == MenuEnterKey.Key && SelectedMenuItem == NewGameMenu)
            {
                StartGame();
            }
            /* Back To Main Menu Screen */
            else if(c == BackMenuKey.Key || c == MenuEnterKey.Key && SelectedMenuItem == BackMenu)
            {
                CurrentMenu = MainMenu;
            }
            else if(c == OptionsMenuKey.Key || c == MenuEnterKey.Key && SelectedMenuItem == OptionsMenu)
            {
                CurrentMenu = OptionsMenu;
            }
            else if(c == DebugToggleKey.Key || c == MenuEnterKey.Key && SelectedMenuItem == ToggleDebugMenu)
            {
                if(ShowDebugInfo)
                {
                    ShowDebugInfo = false;
                }
                else
                {
                    ShowDebugInfo = true;
                }
            }
            /* Select Next Menu Item */
            else if(c == MenuDownKey.Key || c == (ConsoleKey)ConsoleKey.DownArrow)
            {
                int Selected = MenuItems.IndexOf(SelectedMenuItem);
                if (Selected < MenuItems.Count - 1)
                {
                    SelectedMenuItem = MenuItems[Selected + 1];
                }
                else
                {
                    SelectedMenuItem = MenuItems[0];
                }
            }
            /* Select Previous Menu Item */
            else if(c == MenuUpKey.Key || c == (ConsoleKey)ConsoleKey.UpArrow)
            {
                int Selected = MenuItems.IndexOf(SelectedMenuItem);
                if (Selected <= MenuItems.Count - 1 && Selected != 0)
                {
                    SelectedMenuItem = MenuItems[Selected - 1];
                }
                else
                {
                    SelectedMenuItem = MenuItems[MenuItems.Count - 1];
                }
            }
        }

        /* -------------------------------------------------- */

        /* Wait for Game Input */
        static void DealWithUserGameInput(ConsoleKey c)
        {
            /* Back To Main Menu Screen */
            if(c == QuitKey.Key)
            {
                CurrentMenu = MainMenu;
            }
            /* Player Movement */
            else if(c == ConsoleKey.W && PlayerY > 2)
            {
                PlayerY--;
            }
            else if(c == ConsoleKey.A && PlayerX > -2)
            {
                PlayerX--;
            }
            else if(c == ConsoleKey.S && PlayerY < WinY - 3)
            {
                PlayerY++;
            }
            else if(c == ConsoleKey.D && PlayerX < WinX - 7)
            {
                PlayerX++;
            }
        }

        /* -------------------------------------------------- */

        /* After having pressed the NewGameKey.Key */
        static void StartGame()
        {
            Console.Clear();
            CurrentMenu = GamePlayScreen;

            PlayerX = 10;
            PlayerY = WinY / 2;

            do
            {
                Console.Clear();
                DrawGamePlayScreen();
                DrawDebugInfo();
                DrawPlayer(PlayerX, PlayerY);
                Thread.Sleep(10);
                DealWithUserGameInput((System.ConsoleKey)PromptUserInput());
            }
            while(CurrentMenu == GamePlayScreen);

        }

        /* -------------------------------------------------- */

        /* Draw Player */
        static void DrawPlayer(int x, int y)
        {
            Console.SetCursorPosition(PlayerX + 4, PlayerY - 1);
            Console.Write("\\");
            Console.SetCursorPosition(PlayerX + 3, PlayerY - 1);
            Console.Write(">");
            Console.SetCursorPosition(PlayerX + 4, PlayerY);
            Console.Write("|");
            Console.SetCursorPosition(PlayerX + 5, PlayerY);
            Console.Write(">");
            Console.SetCursorPosition(PlayerX + 4, PlayerY + 1);
            Console.Write("/");
            Console.SetCursorPosition(PlayerX + 3, PlayerY + 1);
            Console.Write(">");
        }

        /* -------------------------------------------------- */

        /* Draw Debug Info */
        static void DrawDebugInfo()
        {
            if(ShowDebugInfo)
            {
                /* Debug info showing console XY */
                Console.SetCursorPosition(0, 0);
                Console.WriteLine("/----------------------------------------\\");
                Console.WriteLine("|Debug:");
                Console.WriteLine("|WinX = " + WinX);
                Console.WriteLine("|WinY = " + WinY);
                Console.WriteLine("|PlayerX = " + PlayerX);
                Console.WriteLine("|PlayerY = " + PlayerY);
                Console.WriteLine("|Current Screen = " + CurrentMenu.Name);
                Console.WriteLine("\\----------------------------------------/");
                for(int i = 0; i < 6; ++i)
                {
                    Console.SetCursorPosition(41, 1 + i);
                    Console.WriteLine("|");
                }
                Console.CursorVisible = true;
            }
            else
            {
                Console.CursorVisible = false;
            }
        }
        /* -------------------------------------------------- */

        /* Draw GamePlayScreen */
        static void DrawGamePlayScreen()
        {
            for(int y = 0; y < WinY; ++y)
            {
                for(int x = 0; x < WinX; ++x)
                {
                    Console.SetCursorPosition(x, y);

                    /* NW & SE Corner */
                    if(x == 0 && y == 0 || x == WinX -1 && y == WinY -1)
                    {
                        Console.Write("/");
                    }
                    /* NE & SW Corner */
                    else if(x == 0 && y == WinY - 1 || x == WinX -1 && y == 0)
                    {
                        Console.Write("\\");
                    }
                    /* North & South Rows */
                    else if(y == 0 || y == WinY - 1)
                    {
                        Console.Write("=");
                    }
                    /* West & East Columns */
                    else if(x == 0 || x == WinX -1)
                    {
                        Console.Write("|");
                    }
                }
            }
        }

        /* ---[ Functions ]--- */
        /* -------------------------------------------------- */
    }
}
